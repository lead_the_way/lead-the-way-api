# Lead the Way

An API powered by Vapor and Vapor Cloud to complement the Lead the Way iOS application.

# Install (macOS)

First, ensure that Xcode is installed.
Xcode is generally installed via the Mac App Store.
After Xcode is installed it must be ran at least once.
This ensures that command line developer tools are installed as well.

Homebrew must also be installed.
This can be accomplished by running the following command inside of a Terminal:

```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Vapor Toolbox is the next program that must be installed.
Running the following command in a Terminal:

```bash
brew install vapor/tap/vapor
```

Now with the dependecies installed, to clone the actually project do the following:

```bash
git clone git@gitlab.com:lead_the_way/lead-the-way-api.git
cd lead-the-way-api
vapor xcode
```

Vapor will ask to "Open Xcode project?" Responding `y` will open the project in Xcode.

# Building and Running

In order to build and run the API via Xcode, ensure that "Run" is selected as the scheme and "My Mac" is selected as the target.
Then click on the build and run button (the play icon in the upper left).

Alternatively, when not using Xcode, building and running can be done through a Terminal.
Running `vapor build` from within the project directory will build the project with the default settings.
Then running `vapor run` will run the project with the default settings.

The project has three environments:
* Production
* Development
* Local

Each environment will use a different database configuration.
`Production` will use the Vapor Cloud database, `Development` will use the Google Cloud database, and `Local` will use a local MySQL database running on your machine.
By default when running on a local machine, the environment is set to `Development`.
When running on Vapor Cloud the environment is automatically chosen.
Choosing the `Production` environment should fail to connect to the database, since the credentials are actually stored on the Vapor Cloud instance.

In order to specify your environment while using Xcode, follow these steps:
* Select `Product` -> `Scheme` -> `Edit Scheme...`
* On the left ensure that `Run` is selected
* On the top bar select `Arguments`
* For `Arguments Passed On Launch` click the `+` button
* Add `--env production`, `--env development`, or `--env local`
* Select `Product` -> `Run`

To specify your environment while using the command line, follow these steps:
* Navigate to the project directory
* Run `vapor build`
* Run `vapor run --env=production`, `vapor run --env=development`, or `vapor run --env=local`

When using the `Local` environment the following MySQL credentials should be used for your local MySQL instance:
```
hostname = "localhost"
port = 3306
username = "root"
password = "password"
database = "vapor"

```

When running the application (either via Xcode or the command line) a statement should print out to the console specifying the detected environment, e.g. `Current environment: local`.

# Building and Running Problems

If switching branches does not seem to update the project (e.g. files are seemingly missing, files are not updated, et cetera), it may be necessary to rebuild the `.xcodeproj` file.
This can be easily accomplished by running `vapor xcode` from within the project directory.

# How to use the API

**These are the available end points**
* domain: dev.leadtheway.app 

*  **GET**
   * /building/ {building_id}
   * /floor/ {floor_id}

* **POST** *( all parameters most be put in the request body )*
  * /building
    * **Parameters**
      * name
      * longitude
      * latitude
  * /floor
    * **Parameters**
      * elevation
      * buildingID
      * name
      * floorNumber
  * /obstruction
    * **Parameters**
      * floorID
  * /destination
    * **Parameters**
      * x
      * y
      * floorID
      * description

# Installing SwiftLint

SwiftLint is a tool that will allow us to enforce Swift style and conventions.

First we need to install and set up a couple of things:
- Homebrew (if not installed already)
- Cocoapods
  - `brew install cocoapods` this will install cocoapods
  - `pod setup --verbose` this will pull the repository for cocoapods and do some setting up
  - Since you already have the Podfile in the directory initializing it is not neccesary
  - Run `pod install`
  - After you run this close the project and open the project using the `.xcworkspace` file
- SwiftLint
  - Make sure that you open your project as an `.xcworkspace` and not `.xcodeproj`
  - highlight the LeadTheWay project -> click on Build Phases
  - go to editor(on toolbar) -> Add Build Phase -> Add Run Script Build Phase
  - In Run Script under `Shell /bin/sh` insert the following code `"${PODS_ROOT}/SwiftLint/swiftlint"`
