import Foundation
import Vapor
import Fluent
import FluentMySQL

class ObstructionController: RouteCollection {

    func boot(router: Router) throws {
        let obstructionRoutes = router.grouped("obstruction")
        obstructionRoutes.get(Int.parameter, use: addObstruction)
    }

    func getObstructions(for floor: Floor, _ req: Request) throws -> Future<[(Obstruction, Point)]> {
        return try floor.obstructions.query(on: req).join(\Point.obstructionID, to: \Obstruction.id)
            .alsoDecode(Point.self).all()
    }

    func addObstruction(_ req: Request) throws -> Future<Obstruction> {
        return try req.content.decode(Obstruction.self).create(on: req)
    }

}
