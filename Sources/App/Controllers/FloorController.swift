import Foundation
import Vapor
import Fluent
import FluentMySQL

class FloorController: RouteCollection {

    func boot(router: Router) throws {
        let floorRoutes = router.grouped("floor")
        floorRoutes.get(Int.parameter, use: getDetail)
    }

    func getDetail(_ req: Request) throws -> Future<FloorDetail> {
        let floorId = try req.parameters.next(Int.self)
        let obstructionController = ObstructionController()
        let destinationController = DestinationController()

        return Floor.query(on: req).filter(\Floor.id == floorId).first().flatMap { floor in
            guard let floor = floor else {
                throw Abort(HTTPResponseStatus.internalServerError)
            }

            let floorDetail = FloorDetail(for: floor)
            return try destinationController.getDestinations(for: floor, req)
                .and(obstructionController.getObstructions(for: floor, req)).flatMap { destinations, obstructions in
                // Assigns the destinations.
                floorDetail.destinations = destinations.map { destination in
                        return DestinationDetail(destination: destination)
                    }
                let obstructionsWithPoints = obstructions // Grabs the obstruction that has a list of points.

                var obstructionDetails = [ObstructionDetail]()

                for (obstruction, point) in obstructionsWithPoints {
                    /*
                        Since each obstruction can have more than one point, if already accountedfor the new point
                        observed will be added to that obstruction, Otherwise a new obstruction will be created with
                        its corresponding point.
                    */
                    let foundObstructionDetail = obstructionDetails.filter {
                        $0.id == obstruction.id
                    }.first

                    if let foundObstructionDetail = foundObstructionDetail {
                        foundObstructionDetail.points?.append(point)
                    } else {
                        let obstructionDetail = ObstructionDetail(for: obstruction)
                        obstructionDetail.points = [Point]()
                        obstructionDetail.points?.append(point)
                        obstructionDetails.append(obstructionDetail)
                    }
                }
                floorDetail.obstructions = obstructionDetails
                return Future.map(on: req) {
                    return floorDetail
                }
            }
        }
    }

    func addFloor(_ req: Request) throws -> Future<Floor> {
        let newFloor = try req.content.decode(Floor.self)
        return newFloor.create(on: req)
    }

}
