import Foundation
import Vapor
import Fluent
import FluentMySQL

class TestController: RouteCollection {
    public let env: Environment
    
    
    func boot(router: Router) throws {
        let testRoutes = router.grouped("test").grouped(SessionsMiddleware.self)
        testRoutes.get(use: checkTestBuildingAvailability)
    }
    
    init(_ env: Environment) {
        self.env = env
    }
    
    func checkTestBuildingAvailability(_ req: Request) throws -> Future<String> {
        return Building.find(1, on: req).flatMap { building in
            if let _ = building {
                return Future.map(on: req) {
                    return "Building has already been created."
                }
            }
            else {
                try self.getTestBuildingFile(req).do { buildingDetail in
                    do {
                    try self.insertTestBuilding(for: buildingDetail, req)
                    } catch _ {
                        fatalError("Could not insert test building")
                    }
                }
                return Future.map(on: req) {
                    return "Building is being inserted."
                }
            }
        }
    }
    func insertTestBuilding(for buildingDetail: BuildingDetail, _ req: Request) throws  {

        let building = Building(name: buildingDetail.name, longitude: buildingDetail.longitude,
                                latitude: buildingDetail.latitude)
        
        building.create(on: req).addAwaiter(callback: { building in
            if let buildingId = building.result?.id {
                
                if let floorDetails = buildingDetail.floors {
                    
                    for floorDetail in floorDetails {
                        let floor = Floor(elevation: floorDetail.elevation, buildingID: buildingId,
                                          name: floorDetail.name, floorNumber: floorDetail.floorNumber,
                                          width: floorDetail.width, depth: floorDetail.depth)
                        
                        floor.create(on: req).addAwaiter(callback: { floor in
                            if let floorId = floor.result?.id {
                                if let obstructionDetails = floorDetail.obstructions {
                                    for obstructionDetail in obstructionDetails {
                                        let obstruction = Obstruction(floorID: floorId)
                                        
                                        obstruction.create(on: req).addAwaiter(callback: { obstruction in
                                            if let obstructionId = obstruction.result?.id {
                                                if let pointDetails = obstructionDetail.points {
                                                    for pointDetail in pointDetails {
                                                        let point = Point(x: pointDetail.x, y: pointDetail.y,
                                                                          obstructionID: obstructionId)
                                                        point.create(on: req)
                                                    }
                                                }
                                            }
                                        })
                                    }
                                }
                                
                                if let destinationDetails = floorDetail.destinations {
                                    for destinationDetail in destinationDetails {
                                        let destination = Destination(x: destinationDetail.x, y: destinationDetail.y,
                                                                      floorID: floorId,
                                                                      description: destinationDetail.description,
                                                                      type: destinationDetail.type)
                                        destination.create(on: req)
                                    }
                                }
                            }
                        })
                    }
                }
            }
        })
    }
    
    func getTestBuildingFile(_ req: Request) throws -> Future<BuildingDetail> {
        var hostName = "localhost"
        
        switch(env.name) {
        case "production":
            hostName = "leadtheway.app"
        case "local":
            hostName = "localhost"
        default:
            hostName = "dev.leadtheway.app"
            
        }
        // Connect a new client to the supplied hostname.
        return HTTPClient.connect(hostname: hostName, on: req).flatMap { (client)  in
            // Create an HTTP request: GET /
            let httpReq = HTTPRequest(method: .GET, url: "/testBuilding.json")
            // Send the HTTP request, fetching a response
            return client.send(httpReq).map { response in
                guard let data = response.body.data else {
                    fatalError("No data found.")
                }
                do {
                    return try JSONDecoder().decode(BuildingDetail.self, from: data)
                }
                catch _ {
                    fatalError("Error decoding")
                }
            }
        }
    }
}
