import Foundation
import Vapor
import Fluent
import FluentMySQL

class BuildingController: RouteCollection {
    
    func boot(router: Router) throws {
        let buildingRoutes = router.grouped("building").grouped(SessionsMiddleware.self)
        buildingRoutes.post(use: addBuilding)
        buildingRoutes.get(Int.parameter, use: getFloors)
        buildingRoutes.get("within", Double.parameter, Double.parameter, Double.parameter, use: getLocation)
    }
    
    func getLocation(_ req: Request) throws -> Future<[Building]> {
        let latitude = try req.parameters.next(Double.self)
        let longitude = try req.parameters.next(Double.self)
        let range = try req.parameters.next(Double.self)
        let minLatitude = latitude - range
        let maxLatitude = latitude + range
        let minLongitude = longitude - range
        let maxLongitude = longitude + range
        
        return Building.query(on: req)
            .filter(\Building.latitude >= minLatitude)
            .filter(\Building.latitude <= maxLatitude)
            .filter(\Building.longitude >= minLongitude)
            .filter(\Building.longitude <= maxLongitude)
            .all()
    }
    
    func getFloors(_ req: Request) throws -> Future<BuildingDetail> {
        let name = try req.session().id
        
        guard let buildingId = Optional(try req.parameters.next(Int.self)) else {
            throw Abort(HTTPResponseStatus.noContent)
        }
        return Building.query(on: req).filter(\Building.id == buildingId).first().flatMap { building in
            guard let building = building else {
                throw Abort(HTTPResponseStatus.internalServerError)
            }
            return try building.floors.query(on: req).all().flatMap { floors in
                return Future.map(on: req) {
                    let floorDetails = floors.map { floor in
                        return FloorDetail(for: floor)
                    }
                    return BuildingDetail(for: building, with: floorDetails)
                }
            }
        }
    }
    
    func addBuilding(_ req: Request) throws -> Future<Building> {
        return try req.content.decode(Building.self).create(on: req)
    }
    
}
