import Foundation
import Vapor
import Fluent
import FluentMySQL

class DestinationController: RouteCollection {

    func boot(router: Router) throws {
        let destinationRoutes = router.grouped("destination")
        destinationRoutes.post(use: addDestination)
    }

    func getDestinations(for floor: Floor, _ req: Request) throws -> Future<[Destination]> {
        return try floor.destinations.query(on: req).all()
    }

    func addDestination(_ req: Request) throws -> Future<Destination> {
        return try req.content.decode(Destination.self).create(on: req)
    }

}
