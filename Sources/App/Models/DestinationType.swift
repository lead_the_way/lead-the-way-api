//
//  DestinationType
//  Run
//
//  Created by Kevin Santana on 4/7/19.
//

import Vapor
import Fluent
import FluentMySQL

public enum DestinationType: String, MySQLEnumType  {
    public static func reflectDecoded() throws -> (DestinationType, DestinationType) {
        return (.room, .unknown)
    }
    
    case room = "room"
    case entry = "entry"
    case exit = "exit"
    case stairs = "stairs"
    case elevator = "elevator"
    case restroom = "restroom"
    case landmark = "landmark"
    case store = "store"
    case vendingMachine = "vending machine"
    case waterFountain = "water fountain"
    case unknown = "unknown"
}
