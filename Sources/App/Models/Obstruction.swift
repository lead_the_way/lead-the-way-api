import FluentMySQL
import Vapor

final class Obstruction: MySQLModel, MySQLMigration {
    var id: Int?
    var floorID: Int

    init(floorID: Int) {
        self.floorID = floorID
    }

}
extension Obstruction {

    var floor: Parent<Obstruction, Floor> {
        return parent(\.floorID)
    }

    var points: Children<Obstruction, Point> {
        return children(\.obstructionID)
    }

}

/// Allows `Obstruction` to be used as a dynamic migration.
extension Obstruction: Migration { }

/// Allows `Obstruction` to be encoded to and decoded from HTTP messages.
extension Obstruction: Content { }

/// Allows `Obstruction` to be used as a dynamic parameter in route definitions.
extension Obstruction: Parameter { }
