import FluentMySQL
import Vapor

final class Destination: MySQLModel, MySQLMigration {

    var id: Int?
    var x: Int
    var y: Int
    var floorID: Int
    var description: String?
    var type: DestinationType?

    init(x: Int, y: Int, floorID: Int, description: String?, type: DestinationType?) {
        self.x = x
        self.y = y
        self.floorID = floorID
        self.description = description
        self.type = type
    }

}

extension Destination {

    var floor: Parent<Destination, Floor> {
        return parent(\.floorID)
    }

}

/// Allows `Destination` to be used as a dynamic migration.
extension Destination: Migration { }

/// Allows `Destination` to be encoded to and decoded from HTTP messages.
extension Destination: Content { }

/// Allows `Destination` to be used as a dynamic parameter in route definitions.
extension Destination: Parameter { }
