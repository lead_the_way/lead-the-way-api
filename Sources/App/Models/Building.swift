import FluentMySQL
import Vapor

final class Building: MySQLModel, MySQLMigration {

    var id: Int?
    var name: String?
    var longitude: Double?
    var latitude: Double?

    init(name: String?, longitude: Double?, latitude: Double?) {
        self.name = name
        self.longitude = longitude
        self.latitude = latitude
    }

}

extension Building {

    var floors: Children<Building, Floor> {
        return children(\.buildingID)
    }

}

/// Allows `Building` to be used as a dynamic migration.
extension Building: Migration { }

/// Allows `Building` to be encoded to and decoded from HTTP messages.
extension Building: Content { }

/// Allows `Building` to be used as a dynamic parameter in route definitions.
extension Building: Parameter { }
