import FluentMySQL
import Vapor

final class Point: MySQLModel, MySQLMigration {
    var id: Int?
    let x: Int
    let y: Int
    let obstructionID: Int

    init(x: Int, y: Int, obstructionID: Int) {
        self.x = x
        self.y = y
        self.obstructionID = obstructionID
    }

}

extension Point {

    var obstruction: Parent<Point, Obstruction> {
        return parent(\.obstructionID)
    }

}

/// Allows `Point` to be used as a dynamic migration.
extension Point: Migration { }

/// Allows `Point` to be encoded to and decoded from HTTP messages.
extension Point: Content { }

/// Allows `Point` to be used as a dynamic parameter in route definitions.
extension Point: Parameter { }
