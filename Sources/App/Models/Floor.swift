import FluentMySQL
import Vapor

final class Floor: MySQLModel, MySQLMigration {

    var id: Int?
    var elevation: Double?
    var buildingID: Int
    // At least name or floor number must be set, or both.
    var name: String?
    var floorNumber: Int?
    // This property represents the width of a floor in 2D space.
    var width: Int
    // This property represents the height of a floor in 2D space.
    var depth: Int

    init(elevation: Double?, buildingID: Int, name: String?, floorNumber: Int?, width: Int, depth: Int) {
        self.elevation = elevation
        self.buildingID = buildingID
        self.name = name
        self.floorNumber = floorNumber
        self.width = width
        self.depth = depth
    }

}

extension Floor {

    var building: Parent<Floor, Building> {
        return parent(\.buildingID)
    }

    var obstructions: Children<Floor, Obstruction> {
        return children(\.floorID)
    }

    var destinations: Children<Floor, Destination> {
        return children(\.floorID)
    }

}

/// Allows `Floor` to be used as a dynamic migration.
extension Floor: Migration { }

/// Allows `Floor` to be encoded to and decoded from HTTP messages.
extension Floor: Content { }

/// Allows `Floor` to be used as a dynamic parameter in route definitions.
extension Floor: Parameter { }
