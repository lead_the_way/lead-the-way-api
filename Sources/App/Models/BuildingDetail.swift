import Foundation
import Vapor
import FluentMySQL

final class ObstructionDetail: MySQLModel, MySQLMigration {
    
    var id: Int?
    var floorID: Int
    var points: [Point]?
    init(for obstruction: Obstruction) {
        id = obstruction.id
        floorID = obstruction.floorID
    }

    init(id: Int?, floorID: Int) {
        self.id = id
        self.floorID = floorID
        points = [Point]()
    }

}

final class DestinationDetail: MySQLModel, MySQLMigration {
    var id: Int?
    var x: Int
    var y: Int
    var floorID: Int
    var description: String?
    var type: DestinationType?
    
    init(destination: Destination) {
        id = destination.id
        x = destination.x
        y = destination.y
        floorID = destination.floorID
        description = destination.description
        type = destination.type
    }
    
    init(id: Int?, x: Int, y: Int, floorID: Int, description: String?, type: DestinationType?) {
        self.id = id
        self.x = x
        self.y = y
        self.floorID = floorID
        self.description = description
        self.type = type
    }
}

final class FloorDetail: MySQLModel, MySQLMigration {
    
    var id: Int?
    var elevation: Double?
    var buildingID: Int
    // At least name or floor number must be set, or both.
    var name: String?
    var floorNumber: Int?
    // This property represents the width of a floor in 2D space.
    var width: Int
    // This property represents the height of a floor in 2D space.
    var depth: Int
    var obstructions: [ObstructionDetail]?
    var destinations: [DestinationDetail]?

    init(for floor: Floor) {
        id = floor.id
        elevation = floor.elevation
        buildingID = floor.buildingID
        name = floor.name
        floorNumber = floor.floorNumber
        width = floor.width
        depth = floor.depth
    }

    init(id: Int?, elevation: Double?, buildingID: Int, name: String?, floorNumber: Int?, width: Int, depth: Int) {
        self.id = id
        self.elevation = elevation
        self.buildingID = buildingID
        self.name = name
        self.floorNumber = floorNumber
        self.width = width
        self.depth = depth
        obstructions = [ObstructionDetail]()
        destinations = [DestinationDetail]()
    }

}

final class BuildingDetail: MySQLModel, MySQLMigration {
    
    var id: Int?
    var name: String?
    var longitude: Double?
    var latitude: Double?
    var floors: [FloorDetail]?
    
    init(for building: Building, with floors: [FloorDetail]) {
        id = building.id
        name = building.name
        longitude = building.longitude
        latitude = building.latitude
        self.floors = floors
    }

    init(id: Int?, name: String?, longitude: Double?, latitude: Double?) {
        floors = [FloorDetail]()
        self.id = id
        self.name = name
        self.longitude = longitude
        self.latitude = latitude
    }
    
}

extension BuildingDetail: Content { }

extension FloorDetail: Content { }
