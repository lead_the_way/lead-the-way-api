import FluentMySQL
import Vapor
import Authentication

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    // Register providers first.
    try services.register(FluentMySQLProvider())
    try services.register(AuthenticationProvider())

    // Register routes to the router.
    let router = EngineRouter.default()
    try routes(router, env)
    services.register(router, as: Router.self)
    /*
        Creates a 'CommandConfig' with default commands.Adds the revert command which is called on every model that
        conforms to the  MySQLMigration protocol. Revert is used to delete any changes made to the database. This
        allows us to make changes to the database and deploy them without having to login into the database and remove
        those tables ourselves.
    */
    var commandConfig = CommandConfig.default()
    commandConfig.use(RevertCommand.self, as: "revert")
    services.register(commandConfig)
    
    config.prefer(MemoryKeyedCache.self, for: KeyedCache.self)

    // Register middleware.
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    // middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(SessionsMiddleware.self)
    middlewares.use(FileMiddleware.self)
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)

    // Create the MySQL database.
    let mySQLDatabase = setupMySQLDatabase(env)

    // Register the configured MySQL database to the database config.
    var databases = DatabasesConfig()
    databases.add(database: mySQLDatabase, as: .mysql)
    services.register(databases)

    // Configure migrations.
    var migrations = MigrationConfig()

    // Models to add along with their respective migration options.
    migrations.add(model: Building.self, database: .mysql)
    migrations.add(model: Floor.self, database: .mysql)
    migrations.add(model: Obstruction.self, database: .mysql)
    migrations.add(model: Destination.self, database: .mysql)
    migrations.add(model: Point.self, database: .mysql)

    services.register(migrations)
}

/// Create a MySQL Database based on testing, environment, or default options.
private func setupMySQLDatabase(_ env: Environment) -> MySQLDatabase {
    var hostname: String
    var port: Int
    var username: String
    var password: String
    var database: String
    var transport: MySQLTransportConfig

    print("Current environment: \(env.name)")

    switch env.name {
    case "production":
        // Try to get Vapor Cloud credentials through environment variables.
        // If not avilable then use defaults (same as local).
        hostname = Environment.get("DATABASE_HOSTNAME") ?? "localhost"
        port = Int(Environment.get("DATABASE_PORT") ?? "3306") ?? 3306
        username = Environment.get("DATABASE_USER") ?? "root"
        password = Environment.get("DATABASE_PASSWORD") ?? "password"
        database = Environment.get("DATABASE_DB") ?? "vapor"
        transport = .cleartext
    case "local":
        // Use a local database (this is for testing).
        hostname = "localhost"
        port = 3306
        username = "root"
        password = "password"
        database = "vapor"
        transport = .unverifiedTLS
    default:
        // Use Google Cloud SQL credentials.
        hostname = "35.226.6.69"
        port = 3306
        username = "root"
        password = "HaAgurLpnJi4fBov"
        database = "vapor"
        transport = .cleartext
    }

    let databaseConfig = MySQLDatabaseConfig(hostname: hostname, port: port, username: username,
                                             password: password, database: database, transport: transport)

    return MySQLDatabase(config: databaseConfig)
}
