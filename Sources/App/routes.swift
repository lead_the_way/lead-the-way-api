import Vapor
/// Register your application's routes here.
public func routes(_ router: Router, _ env: Environment) throws {
    let buildingController = BuildingController()
    try router.register(collection: buildingController)

    let floorController = FloorController()
    try router.register(collection: floorController)

    let obstructionController = ObstructionController()
    try router.register(collection: obstructionController)

    let destinationController = DestinationController()
    try router.register(collection: destinationController)
    
    let testController = TestController(env)
    try router.register(collection: testController)
}
